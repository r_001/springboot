package com.ruchir.Test.Controller;

import com.ruchir.Test.Pojo.User;
import com.ruchir.Test.Service.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;




@RestController
public class TestController {


    @Autowired
    Test test;

    @PostMapping ("/user")
    public List<User> createUser(@RequestBody User user){
        System.out.println(user.getId());
        return test.getUserList(user);
    }
}
